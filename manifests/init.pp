# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include gitlab_profile
class gitlab_profile (
  $external_url,
  $letsencrypt,
  $gitlab_rails,
){
  class { 'gitlab':
    external_url => $external_url,
    letsencrypt  => $letsencrypt,
    gitlab_rails => $gitlab_rails,
  }
}
